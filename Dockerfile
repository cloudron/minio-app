FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=minio/minio versioning=regex:^RELEASE\.(?<major>\d{4})-(?<minor>\d{2})-(?<patch>\d{2})T(?<build>.+)
ARG MINIO_VERSION=RELEASE.2025-03-12T18-04-18Z

RUN wget https://dl.min.io/server/minio/release/linux-amd64/archive/minio.${MINIO_VERSION} -O /app/code/minio && chmod +x /app/code/minio

# Latest CLI
RUN wget https://dl.min.io/client/mc/release/linux-amd64/mc -O /app/code/mc && chmod +x /app/code/mc

COPY env.sh.template start.sh /app/code/

CMD [ "/app/code/start.sh" ]
