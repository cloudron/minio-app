<nosso>
Please use the following credentials to login:

**Username**: minioadmin<br/>
**Password**: minioadmin<br/>

</nosso>

<sso>

Please use the following credentials to login via 'Other Authentication Methods' -> 'Use Credentials':

**Username**: minioadmin<br/>
**Password**: See `MINIO_ROOT_PASSWORD` in `/app/data/env.sh` <a href="/frontend/filemanager.html#/viewer/app/$CLOUDRON-APP-ID/env.sh">Open File Manager</a><br/>

Cloudron users have `readwrite` access policy. See the [docs](https://cloudron.io/documentation/apps/minio/#admin-credentials) on how to change it.

</nosso>
