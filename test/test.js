#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    timers = require('timers/promises'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    const BUCKET = 'cloudrontestbucket';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;
    let rootPassword;
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password, expandLoginForm=true) {
        await browser.manage().deleteAllCookies();
        await browser.get('about:blank');
        await browser.sleep(2000);
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(2000);

        if (expandLoginForm) {
            await waitForElement(By.xpath('//div[@id="alternativeMethods-select"]/div[contains(., "Other Authentication Methods")]'));
            await browser.findElement(By.xpath('//div[@id="alternativeMethods-select"]/div[contains(., "Other Authentication Methods")]')).click();
            await browser.sleep(2000);
            await browser.findElement(By.xpath('//li[contains(., "Use Credentials")] | //div[@label="Use Credentials"]')).click();
            await browser.sleep(2000);
        }
        await waitForElement(By.id('accessKey'));
        await browser.findElement(By.id('accessKey')).sendKeys(username);
        await browser.findElement(By.id('secretKey')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@id="do-login"]')).click();
        await waitForElement(By.xpath('//span[contains(text(), "Buckets")]'));
        await timers.setTimeout(5000);
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/login`);
        await browser.sleep(10000);

        await waitForElement(By.xpath('//button[contains(., "iam")]'));
        await browser.findElement(By.xpath('//button[contains(., "iam")]')).click();
        await browser.sleep(10000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
            
            await browser.sleep(2000);
        }

        await waitForElement(By.xpath('//span[contains(text(), "Buckets")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/`);
        await waitForElement(By.xpath('//span[contains(text(), "Buckets")]'));
        const button = await browser.findElement(By.xpath('//button[@id="sign-out"]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await button.click();
        await browser.sleep(10000); // needed!
        await waitForElement(By.xpath('//*[@id="accessKey"] | //button[contains(., "Cloudron")]'));
    }

    async function addBucket() {
        await browser.get(`https://${app.fqdn}/buckets`);
        await waitForElement(By.xpath('//button[@id="create-bucket"]'));
        await browser.findElement(By.xpath('//button[@id="create-bucket"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@id="bucket-name"]')).sendKeys(BUCKET);
        await browser.findElement(By.xpath('//button[@id="create-bucket"]')).click();
        await waitForElement(By.xpath(`//h1[contains(text(), "${BUCKET}")]`));
        await timers.setTimeout(5000);
    }

    async function checkBucket() {
        await browser.get(`https://${app.fqdn}/buckets`);
        await waitForElement(By.xpath(`//h1[contains(text(), "${BUCKET}")]`));
    }

    async function checkRedirect() {
        const response = await superagent.get(`https://${app.secondaryDomains[0].fqdn}`).set('User-Agent', 'Mozilla/5.0').redirects(0).ok(() => true);
        expect(response.status).to.be(307);
        expect(response.headers.location).to.be(`https://${app.fqdn}`);
    }

    async function checkApi() {
        const response = await superagent.get(`https://${app.secondaryDomains[0].fqdn}`).ok(() => true);
        expect(response.status).to.be(403);
        expect(response.body.toString('utf8')).to.contain('<Code>AccessDenied</Code>');
    }

    async function changeAdminCredentials() {
        let data = fs.readFileSync(path.join(__dirname, '../env.sh.template'), 'utf8');
        data += '\nexport MINIO_ROOT_USER=minioakey\nexport MINIO_ROOT_PASSWORD=minioskey\n';
        fs.writeFileSync('/tmp/env.sh', data);
        execSync(`cloudron push --app ${app.id} /tmp/env.sh /app/data/env.sh`, EXEC_ARGS);
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(10000);
    }

    async function getAdminCredentials() {
        execSync(`cloudron pull --app ${app.id} /app/data/env.sh /tmp/env.sh`, EXEC_ARGS);
        const data = fs.readFileSync('/tmp/env.sh', 'utf8');
        const m = data.match(/MINIO_ROOT_PASSWORD=(.*)/);
        if (!m) throw new Error('Could not detect root password');
        rootPassword = m[1].trim();
        console.log(`root password is [${rootPassword}]`);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // // no SSO
    it('install app (no SSO)', async function () {
        execSync(`cloudron install --no-sso --location ${LOCATION} --secondary-domains API_SERVER_DOMAIN=${LOCATION}-api`, EXEC_ARGS);
        await timers.setTimeout(10000);
    });

    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'minioadmin', 'minioadmin', false));
    it('can add bucket', addBucket);
    it('can logout', logout);
    it('does redirect', checkRedirect);
    it('check api', checkApi);

    it('can change admin credentials', changeAdminCredentials);
    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(10000);
    });

    it('can admin login', login.bind(null, 'minioakey', 'minioskey', false));
    it('has bucket', checkBucket);
    it('can logout', logout);
    it('does redirect', checkRedirect);
    it('check api', checkApi);
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // SSO
    it('install app (SSO)', async function () {
        execSync(`cloudron install --location ${LOCATION} --secondary-domains API_SERVER_DOMAIN=${LOCATION}-api`, EXEC_ARGS);
        await timers.setTimeout(10000);
    });

    it('can get app information', getAppInfo);
    it('can get admin credentials', getAdminCredentials);
    it('can admin login', async function () { await login('minioadmin', rootPassword); });
    it('can add bucket', addBucket);
    it('can logout', logout);
    it('does redirect', checkRedirect);
    it('check api', checkApi);

    it('can OIDC login', loginOIDC.bind(null, username, password, false));
    it('has bucket', checkBucket);
    it('can logout', logout);

    it('can change admin credentials', changeAdminCredentials);

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(10000);
    });

    it('can admin login', login.bind(null, 'minioakey', 'minioskey'));
    it('has bucket', checkBucket);
    it('can logout', logout);
    it('does redirect', checkRedirect);
    it('check api', checkApi);

    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('has bucket', checkBucket);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        await timers.setTimeout(10000);
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, 'minioakey', 'minioskey'));
    it('has bucket', checkBucket);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('has bucket', checkBucket);
    it('can logout', logout);

    it('does redirect', checkRedirect);
    it('check api', checkApi);

    it('move to different location', async function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', EXEC_ARGS);
        await timers.setTimeout(10000);
    });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, 'minioakey', 'minioskey'));
    it('has bucket', checkBucket);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('has bucket', checkBucket);
    it('can logout', logout);

    it('does redirect', checkRedirect);
    it('check api', checkApi);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', function () { execSync('cloudron install --appstore-id io.minio.cloudronapp --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can get admin credentials', getAdminCredentials);
    it('can admin login', async function () { await login('minioadmin', rootPassword); });
    it('can add buckets', addBucket);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('has bucket', checkBucket);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });
    it('can configure', function () { execSync(`cloudron configure --app ${LOCATION} --location ${LOCATION} --secondary-domains API_SERVER_DOMAIN=${LOCATION}-api`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', async function () { await login('minioadmin', rootPassword); });
    it('has bucket', checkBucket);
    it('can logout', logout);

    it('can OIDC login', loginOIDC.bind(null, username, password, true));
    it('has bucket', checkBucket);
    it('can logout', logout);

    it('does redirect', checkRedirect);
    it('check api', checkApi);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});

