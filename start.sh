#!/bin/bash

set -eu

mkdir -p /app/data/data /run/minio/config /run/minio/certs

# env vars take precedence over config.yaml (https://github.com/minio/minio/blob/master/docs/distributed/CONFIG.md#things-to-know)
if [[ ! -f /app/data/env.sh ]]; then
    echo "=> First run"
    cp /app/code/env.sh.template /app/data/env.sh
    # minio does not show the password login by default when OIDC is setup (https://github.com/minio/minio/discussions/16928)
    # we generate a dynamic password because users might forget to change the admin password (with the oidc login being so click friendly)
    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        echo -e "export MINIO_ROOT_USER=minioadmin\nexport MINIO_ROOT_PASSWORD=$(pwgen -1s 20)\n\n" >> /app/data/env.sh
    else
        echo -e "export MINIO_ROOT_USER=minioadmin\nexport MINIO_ROOT_PASSWORD=minioadmin\n\n" >> /app/data/env.sh
    fi
fi

source /app/data/env.sh

# https://docs.min.io/minio/baremetal/reference/minio-server/minio-server.html#envvar.MINIO_SERVER_URL
export MINIO_SERVER_URL="https://${API_SERVER_DOMAIN}"
export MINIO_BROWSER_REDIRECT_URL="https://${CLOUDRON_APP_DOMAIN}"

if [[ ! -d /app/data/mc_config ]]; then
    mkdir -p /app/data/mc_config
    /app/code/mc --config-dir /app/data/mc_config &> /dev/null || true
fi

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    export MINIO_IDENTITY_OPENID_DISPLAY_NAME="${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}"
    export MINIO_IDENTITY_OPENID_CONFIG_URL="${CLOUDRON_OIDC_DISCOVERY_URL}"
    export MINIO_IDENTITY_OPENID_CLIENT_ID="${CLOUDRON_OIDC_CLIENT_ID}"
    export MINIO_IDENTITY_OPENID_CLIENT_SECRET="${CLOUDRON_OIDC_CLIENT_SECRET}"
    export MINIO_IDENTITY_OPENID_SCOPES="openid profile email"
    if [[ -z "${MINIO_IDENTITY_OPENID_ROLE_POLICY:-}" ]]; then
        export MINIO_IDENTITY_OPENID_ROLE_POLICY="readwrite"
    fi

    export MINIO_IDENTITY_OPENID_COMMENT="Cloudron OIDC"
fi

# minio is used for backups at times and has a large number of files. optimize by checking if files are actually in correct chown state
echo "==> Changing ownership"
[[ $(stat --format '%U' /app/data/data) != "cloudron" ]] && chown -R cloudron:cloudron /app/data

echo "==> Starting minio"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/minio --quiet ${CLOUDRON_MINIO_STARTUP_ARGS} --address :9000 --console-address :8000

